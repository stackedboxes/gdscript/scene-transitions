extends SbxsSceneTransition


func init(args: Dictionary) -> void:
	var playbackSpeed := 1.0
	if args.has("duration"):
		playbackSpeed /= args["duration"]
	$AnimationPlayer.playback_speed = playbackSpeed


func _ready() -> void:
	$TextureRect.texture = takeScreenshot()
	yield($AnimationPlayer, "animation_finished")
	queue_free()
