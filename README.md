# Scene Transitions

*Part of [StackedBoxes' GDScript Hodgepodge](https://gitlab.com/stackedboxes/gdscript/)*

Provides scene transition effects. This library works well with my Scene Stack
library -- that's the only way I use it. But they are really meant to be
independent, so you might have some success using these effects without using my
Scene Stack.

Each effect is merely a Godot scene that, when added to the scene tree will
perform the effect and remove itself from the tree when done. By convention,
each transition effect has an `init()` method that accepts a Dictionary argument
usable to configure the effect. All effects support a `"duration"` field
containing the desired duration of the transition in seconds. Other fields
supported are described further down.

**Note:** I am pretty sure this will only work if you place this library under
`scripts/scene-transitions` in your project.

## Included effects

* `FadeOutIn`: Fade out the screen to black, then fade it in to the new scene.
