#
# Scene Transitions
# StackedBoxes' GDScript Hodgepodge
#
# Copyright (c) 2021 Leandro Motta Barros
#

#
# This is a useful base class for scene transition effects, which provides the
# pieces that most effects need. That said, the Scene Stack library doesn't
# really care if your effect inherits from it or not.
#

extends CanvasLayer
class_name SbxsSceneTransition


func init(_args: Dictionary) -> void:
	pass


func takeScreenshot() -> Texture:
	var img := get_viewport().get_texture().get_data()
	img.convert(Image.FORMAT_RGB8)
	img.flip_y()
	var tex := ImageTexture.new()
	tex.create_from_image(img, 0)
	return tex
